var through = require("through2");

module.exports = function(siteParams)
{
    var setPipe = through.obj(
        function(file, encoding, callback)
        {
            file.data = {
                page: file.page || {},
                site: siteParams || {},
                contents: file._contents ? file._contents.toString() : null
            };

            callback(null, file);
        });

    return setPipe;
}
